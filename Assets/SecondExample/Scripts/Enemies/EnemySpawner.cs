using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EnemySpawner : ITickable, IPause
{
    private float _spawnCooldown = 5f;
    private List<Transform> _spawnPoints;

    private List<Enemy> _spawnedEnemies = new List<Enemy>();

    private EnemyFactory _enemyFactory;

    private Coroutine _spawn;

    private bool _isPaused;
    private float _time;

    [Inject]
    private void Construct(EnemyFactory enemyFactory, PauseHandler pauseHandler , List<Transform> spawnPoints)
    {
        _enemyFactory = enemyFactory;
        pauseHandler.Add(this);
        _spawnPoints = new List<Transform>();
        _spawnPoints = spawnPoints;
    }

    public void Tick()
    {
        Debug.Log("Тик запущен из спавнера");

        _time += Time.deltaTime;

        if (_time >= _spawnCooldown)
        {
            Enemy enemy = _enemyFactory.Get((EnemyType)UnityEngine.Random.Range(0, Enum.GetValues(typeof(EnemyType)).Length));
            enemy.MoveTo(_spawnPoints[UnityEngine.Random.Range(0, _spawnPoints.Count)].position);
            _spawnedEnemies.Add(enemy);
            _time = 0;
        }
            
    }

    public void SetPause(bool isPaused)
    {
        _isPaused = isPaused;

        foreach(Enemy enemy in _spawnedEnemies)
            enemy.SetPause(isPaused);   
    }

}
