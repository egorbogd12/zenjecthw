using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EnemySpawnerInstaller : MonoInstaller
{
    [SerializeField] private List<Transform> _spawnPoints;

    public override void InstallBindings()
    {
        BindSpawnPoints();
        
        BindFactory();

        BindSpawner();
    }

    private void BindSpawnPoints()
    {
        Container.Bind<List<Transform>>().FromInstance(_spawnPoints);
    }

    private void BindFactory()
    {
        Container.Bind<EnemyFactory>().AsSingle();
    }

    private void BindSpawner()
    {
        Container.BindInterfacesAndSelfTo<EnemySpawner>().AsSingle();
    }
}